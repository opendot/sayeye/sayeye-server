set BASEDIR="C:\Program Files\Docker\Docker\resources\bin\"

echo "Stopping Containers"
CMD /C "%BASEDIR%docker-compose" -f docker-compose.staging_local.yml down

echo "Migrate"
CMD /C "%BASEDIR%docker-compose" -f docker-compose.staging_local.yml run --rm api ruby bin/rails db:migrate RAILS_ENV=staging_local

echo "Preprocess personal images"
CMD /C "%BASEDIR%docker-compose" -f docker-compose.staging_local.yml run --rm api ruby bin/rails sayeye:preprocess_personal_images RAILS_ENV=staging_local

echo "Starting Container"
CMD /C "%BASEDIR%docker-compose" -f docker-compose.staging_local.yml up -d
