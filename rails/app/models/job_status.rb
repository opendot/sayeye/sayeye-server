class JobStatus < ApplicationRecord
  enum status: { queued: 0, performing: 1, complete: 2, failed: 3 }

  belongs_to :associated_obj, polymorphic: true, optional: true

  def self.find_or_create_for_job(job)
    if status = JobStatus.find_by(job_id: job.job_id)
      status.status = :queued
    else
      status = JobStatus.new job_id: job.job_id, job_type: job.class.name
    end

    yield status

    status.save!
    status
   end

   def result_path
     Pathname self.result
   end

   def result_path=(val)
     self.result = val
   end
end
