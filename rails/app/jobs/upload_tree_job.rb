class UploadTreeJob < ApplicationJob
  queue_as :default

  # JobStatus tracking this job
  attr_accessor :status

  def update_status(status)
    if @status
      @status.status = status
      @status.save!
    else
      @status = JobStatus.find_or_create_for_job self do |js|
        js.status = status
      end
    end
  end

  before_enqueue do |job|
    job.update_status :queued
  end

  before_perform do |job|
    job.update_status :performing
  end

  after_perform do |job|
    job.update_status :complete
    File.unlink job.arguments[0] if File.exists? job.arguments[0]
  end

  rescue_from(StandardError) do |exception|
    update_status :failed
    File.unlink @arguments[0] if File.exists? @arguments[0]
    raise exception
  end

  def create_content(params, zip)
    id = params[:id]

    if Content.exists?(id) then
      return Content.find(id)
    end

    params[:content] = get_file zip, id, params[:content]
    params[:content_thumbnail] = get_file zip, id, params[:content_thumbnail]
    params[:thumb] = get_file zip, id, params[:thumb]
    params.delete :thumb

    Content.create! params
  end

  def create_card(params, zip)
    id = params[:id]

    if Card.exists?(id) then
      return Card.find(id)
    end

    params[:selection_sound] = get_file zip, id, params[:selection_sound]
    params[:card_tags] = params.delete(:tags).map{|t| get_or_create_tag CardTag, t}

    Card.create! params
  end

  def get_or_create_tag(c, tag)
    c.find_by(tag: tag) or c.create! id: SecureRandom.uuid, tag: tag
  end

  def fix_page_id(h, k)
    h[k] &&= @page_ids[h[k]]
  end

  def create_page(params)
    page_params = params[:page]
    old_id = page_params.delete :id

    page_params[:id] = SecureRandom.uuid
    page_params[:page_tags] = page_params.delete(:tags).map{|t| get_or_create_tag PageTag, t}
    @page_ids.each {|old, new| page_params[:ancestry][old] &&= new} if page_params[:ancestry]

    page = Page.create! params[:page]
    @page_ids[old_id] = page.id
  end

  def create_layouts(params)
    params[:layouts].map do |layout_params|
      layout_params[:id] = SecureRandom.uuid
      fix_page_id layout_params, :page_id
      fix_page_id layout_params, :next_page_id

      PageLayout.create! layout_params
    end
  end

  def create_tree(params)
    params[:id] = SecureRandom.uuid
    params[:type] = "CustomTree"
    fix_page_id params, :root_page_id
    fix_page_id params, :presentation_page_id
    fix_page_id params, :strong_feedback_page_id
    fix_page_id params, :home_page_id

    tree = Tree.create! params

    User.all.each do |user|
      UserTree.create! id: SecureRandom.uuid, tree: tree, user: user
    end

    tree
  end

  def perform(zip_file_path)
    Tree.transaction do
      Zip::File.open(zip_file_path) do |zip|
        json = zip.read "index.json"
         json = JSON.parse json, symbolize_names: true

        @page_ids = {}

        json[:contents].each {|params|
          create_content params, zip
        }

        json[:cards].each {|params|
          create_card params, zip
        }

        json[:pages].reverse.each {|params|
          create_page params
        }

        json[:pages].each {|params|
          create_layouts params
        }

        tree = create_tree json[:tree]
        @status.associated_obj = tree
      end
    end

    @status
  end

  def get_file(zip, id, urlobj)
    return nil unless urlobj and urlobj[:url]
    uri = URI.parse(urlobj[:url])

    tmpname = [File.basename(uri.path, '.*'), File.extname(uri.path)]
    Tempfile.open(tmpname, encoding: 'ascii-8bit') do |file|
      zip.get_input_stream("#{id}/#{File.basename(uri.path)}") {|s|
        file.write(s.read)
      }
      file.rewind
      file
    end
  end
end
