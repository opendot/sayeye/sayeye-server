include ActiveModelSerializers

class DownloadTreeJob < ApplicationJob
  queue_as :default

  # JobStatus tracking this job
  attr_accessor :status

  def update_status(status)
    if @status
      @status.status = status
      @status.save!
    else
      @status = JobStatus.find_or_create_for_job self do |js|
        js.associated_obj = Tree.find(@arguments[0])
        js.status = status
      end
    end
  end

  before_enqueue do |job|
    job.update_status :queued
  end

  before_perform do |job|
    job.update_status :performing
  end

  after_perform do |job|
    job.update_status :complete
  end

  rescue_from(StandardError) do |exception|
    update_status :failed
    raise exception
  end

  def walk_page(page)
     page.page_layouts.each do |layout|
       @cards.add layout.card

      walk_page layout.next_page if layout.next_page
     end

     # important: add child pages first
     @pages.push page
  end

  def perform(tree_id)
    @tree = Tree.find(tree_id)
    @cards = Set[]
    @pages = [] # ordered from the leaves up

    walk_page @tree.root_page

    pages = @pages.map {|page|
      # SerializableResource.new(page, serializer: PageExportSerializer).to_json
      <<~JSON
        {
          "page": #{page.to_json},
          "layouts": [
            #{page.page_layouts.map(&:to_json).join ",\n      "}
          ]
        }
      JSON
    }
    tree = @tree.to_json

    name = "#{@tree.updated_at.strftime('%F')}_#{@tree.name.gsub(/[^\w]+/, '_')}_#{@tree.id}.zip"
    path = Rails.public_path.join("trees", name)
    Zip::OutputStream.open(path) do |zip|
      zip.put_next_entry "index.json"
      zip.write <<~JSON
        {
          "tree": #{tree},
          "cards": [
            #{@cards.map(&:to_json).join ",\n    "}
          ],
          "contents": [
            #{@cards.map{|c| c.content.to_json}.join ",\n    "}
          ],
          "pages": [
            #{pages.join ",\n    "}
          ]
        }
      JSON

      @cards.each do |card|
        if not card.selection_sound.blank?
          add_uploader zip, card, card.selection_sound
        end

        content = card.content

        if content.respond_to? :content then
          add_uploader zip, content, content.content
        end

        if content.respond_to? :thumb then
          add_uploader zip, content, content.thumb
        end

        if content.respond_to? :content_thumbnail then
          add_uploader zip, content, content.content_thumbnail
        end
      end
    end

    @status.result_path = path.relative_path_from Rails.public_path
    @status
  end

  def add_uploader(zip, content, uploader)
    return if uploader.blank?

    zip.put_next_entry "#{content.id}/#{File.basename(URI.parse(uploader.url).path)}"
    zip.write uploader.read
  end
end
