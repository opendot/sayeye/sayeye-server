class Api::V1::JobStatusesController < ApplicationController
  def show
    js = JobStatus.find params[:id]

    render json: js, status: :ok
  end

  def start_tree_export
    tree = Tree.find params[:id]
    js = JobStatus.where(job_type: DownloadTreeJob.name, associated_obj: tree)
                  .where.not(status: :failed)
                  .order("created_at DESC")
                  .first

    if not js
      js = DownloadTreeJob.perform_later(tree.id).status
    end

    render json: js, status: :ok
  end

  def start_tree_import
    tree = Tempfile.open ['tree-import', '.zip'] do |f|
      IO.copy_stream params[:file].tempfile, f
      f
    end
    js = UploadTreeJob.perform_later(tree.path).status
    render json: js, status: :ok
  end
end
