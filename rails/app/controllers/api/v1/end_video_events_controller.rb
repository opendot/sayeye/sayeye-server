class Api::V1::EndVideoEventsController < Api::V1::SessionEventsController
  def create
    if session_event_params[:card_id].blank?
      render json: {errors: ["#{I18n.t :error_card_id_needed}"]}, status: :bad_request
      return
    end
    super
  end
  
  def broadcast_event_message
    # Not here. Needs to be anticipated in on_event_created
  end

  def render_event(event)
    render json: event, serializer: Api::V1::VideoEventSerializer, status: :created
  end

  def on_event_created
    # End the video.
    ActionCable.server.broadcast("cable_#{ENV['SERVER_TO_DESKTOP_SOCKET_CHANNEL_NAME']}", {type: "END_VIDEO", data: {card_id: session_event_params[:card_id]}}.to_json)
    ActionCable.server.broadcast("cable_#{ENV['SERVER_TO_MOBILE_SOCKET_CHANNEL_NAME']}", {type: "END_VIDEO", data: {card_id: session_event_params[:card_id]}})
    layout = PageLayout.find_by(page_id: params[:page_id], card_id: params[:card_id])
    if layout.nil?
      return false
    end

    if layout.stop?
      ActionCable.server.broadcast("cable_#{ENV['SERVER_TO_MOBILE_SOCKET_CHANNEL_NAME']}", {type: "NO_MORE_PAGES", data: nil})
      #TransitionToEndEvent.create(training_session_id: params[:training_session_id], page_id: params[:page_id])
      return true
    elsif layout.to_home?
      tree = Tree.find_by root_page: layout.page.root
      if tree and tree.home_page
        TransitionToPageEvent.create(next_page_id: tree.home_page_id, training_session_id: params[:training_session_id], page_id: params[:page_id])
        return true
      end
    elsif not layout.next_page_id.blank? and Page.exists?(id: layout.next_page_id)
      TransitionToPageEvent.create(next_page_id: layout.next_page_id, training_session_id: params[:training_session_id], page_id: params[:page_id])
      return true
    end

    TransitionToPageEvent.create(next_page_id: layout.page_id, training_session_id: params[:training_session_id], page_id: params[:page_id])
    return true
  end

  protected

  def session_event_params
    params.permit(:id, :type, :card_id, :page_id, :training_session_id)
  end
end
