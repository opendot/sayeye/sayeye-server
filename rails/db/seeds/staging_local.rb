
require 'ffaker'

# Stop if seed has already been done
if User.exists?(id:"guestUser")
  puts "Database was already seeded. Aborting."
  return
end

puts ""
puts "STAGING LOCAL SEED"

puts ""
puts "Creating guest user and guest patient"
puts "------------------------------------------------------"
guest = GuestUser.create!(id: "guestUser", email: "guest@mail.it", password: ENV["GUEST_USER_PASSWORD"], password_confirmation: ENV["GUEST_USER_PASSWORD"], name: "Guest", surname: I18n.t("user.generic"))
patient_guest = Patient.create!(id: "guestPatient", name: "Guest", surname: I18n.t("patient.generic"), birthdate: DateTime.now)
TobiiCalibrationParameter.create!(id: SecureRandom.uuid(), fixing_radius: 0.05, fixing_time_ms: 600, patient_id: patient_guest.id)
guest.add_patient(patient_guest)
puts "email: #{guest.email}"

puts ""

puts ""
puts "Creating default desktop user"
puts "------------------------------------------------------"
DesktopPc.create(id: "dskpc", email: "desktop@pc.it", password: ENV["DESKTOP_PC_USER_PASSWORD"], password_confirmation: ENV["DESKTOP_PC_USER_PASSWORD"], name: "Desktop", surname: "Pc")
puts "email: desktop@pc.it"
puts "------------------------------------------------------"


# Users are retrieved from the online server
puts "Creating some card tags"
puts "------------------------------------------------------"
card_tags = []

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "fattoria")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "savana")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "gallina")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "mucca")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pecora")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "tigre")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "leone")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "elefante")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "personaggibing")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bing")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "mashaeorso")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "peppafamily")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "masha")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "orso")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "peppa")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "george")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "casa")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "animali")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "mattino")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "giorno")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "notte")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pioggia")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "nuvola")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "sole")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "soleggiato")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "adesso")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "ora")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "oggi")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "io")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "me")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "no... pensa bene")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "maestra")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "insegnante")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "tutor")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "dottore")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "medico")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bambini")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bimbi")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "signora")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "donna")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "signore")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "uomo")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "felice")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "sorridente")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "sorriso")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "triste")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "tristezza")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "infelice")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "arrabbiato")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "rabbia")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "piangere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pianto")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "lacrima")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "spaventato")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "spaventarsi")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "spavento")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "caldo")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "calore")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "capricci")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pestareipiedi")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "ridere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "risata")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "sorridere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "preoccupato")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "nervoso")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "sto bene")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "tutto ok")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "ok")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "no")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "male")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "giù")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "noia")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "annoiato")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "infastidire")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "malato")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "dolore")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "mal di testa")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "stanco")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "sbadigliare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bho")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "nonsapere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "iononso")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "fame")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "mela")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "cibo")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "sete")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "acqua")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "buono")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "delizioso")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "tu")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "lei")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "insieme")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "prenderelamano")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "sgridare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "insultare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "andare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "passeggiare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "camminare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "farsi il bagno")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "farebagno")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "lavarsi")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "lavare i denti")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "lavarsiidenti")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "lavare le mani")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "lavarsilemani")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "lavare i capelli")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "lavarsiicapelli")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "shampoo")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "leggere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "libro")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "lettura")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "mangiare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "mordere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "morso")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "cantare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "musica")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "osservare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "guardare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "correre")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "parlare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "conversare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "chiacchierare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pensare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pensiero")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "penso")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "perdere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "perso")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "sconfitta")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "urlare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "strillare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "picchiare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "picchiarsi")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "schiaffo")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "disegnare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "disegno")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "scrivere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "ballare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "danzare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "ballo")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "aiutare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "aiuto")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "coccole")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "coccolare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "asciugare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "asciugarsi")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "ascoltare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "sentire")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "ascolto")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "scuola")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "studiare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "giocare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "chiesa")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "piscina")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "nuotare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "nuoto")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "parco")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "parco giochi")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "parchetto")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "tog")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "amiciditog")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "mare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bagno")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "lago")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "laghetto")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "montagna")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "monte")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "andarealbagno")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "nuvoloso")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "parzialmentenuvoloso")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "nuvole")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "biscotto")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "dolce")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "frutta")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pera")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "banana")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "arancia")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "fragola")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "verdura")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "verdure")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "gelato")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "cono")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pizza")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "patatine fritte")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "patate")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pane")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pappa")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "darelapappa")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "daredamangiare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "compleanno")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "festa")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "torta")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "candeline")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "natale")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "babbo natale")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "alberodinatale")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "carnevale")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "maschere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "travestirsi")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "soldi")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "euro")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "monete")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "contanti")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "denaro")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "fuoco")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "fiamma")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "falò")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "dente")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "ospedale")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "crocerossa")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "aereo")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "aeroplano")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bici")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bicicletta")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "macchina")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "auto")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "metro")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "metropolitana")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "autobus")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bus")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "nave")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "barca")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "si")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "annuire")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "treno")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "gatto")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "anatra")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "asino")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "balena")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "cane")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "capra")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "cavallo")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "coniglio")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "maiale")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pesce")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "serpente")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "squalo")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "tartaruga")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "topo")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "uccello")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "zebra")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "x")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "caramelle")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "dolci")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "carne")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "carota")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "cioccolato")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "coscia di pollo")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "croissant")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "cornetto")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "brioche")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "formaggio")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "frittata")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "hamburger")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "coca cola")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "ketchup")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "latte")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "limone")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "olio")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "patata")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pasta")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "polpette")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pomodoro")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "popcorn")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "prosciutto")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "riso")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "sale")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "uovo")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "zucchero")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "zucchina")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "minestra")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bimbi e mamma")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bimbo spaventato")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bambino piange")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bambina triste")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bambina piange")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bambini mamma felici")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "mamma")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bimbo felice")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bambina felice")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "mamma saluta binbi")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "mamma esce")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bambini salutano")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "abbracciare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "abbraccio")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "mettere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "metti")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "appoggiare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "prendere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "prendi")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "raccogliere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pulire")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "lavare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "dormire")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "nanna")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "sonno")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "riposino")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "lanciare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "cucinare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "sognare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "incollare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "tagliare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pregare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "avere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "fare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "fareicompiti")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "compiti")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "studio")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "supermercato")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "supermarket")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "parrucchiere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "parrucchieria")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "giardino")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "giardinetto")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "lavoro")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "ufficio")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "lavorare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "neve")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "nevicata")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "nevicando")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "prima")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "dopo")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "ieri")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "domani")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pasqua")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "uovodipasqua")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "festeggiare")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "party")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "carrozzina")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "sedia")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "calcio")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "giocareacalcio")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "nascondere")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "nascondino")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "mascherina")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "virus")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "palla")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "pallone")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bambola")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bambolina")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "bolle di sapone")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "puzzle")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "palloncino")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "peluche")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "orsetto")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "strumenti musicali")
card_tags << tag.id
puts tag.inspect

tag = CardTag.create!(id: SecureRandom.uuid(), tag: "strumenti")
card_tags << tag.id
puts tag.inspect

puts "------------------------------------------------------"

puts ""



puts "Creating some preset cards"
puts "------------------------------------------------------"

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/04d442ff-f199-456b-a3c9-87d195f5e69a.jpeg")))
card = PresetCard.create!(id: "fattoria", label: "Fattoria", level: 3, card_tag_ids: [CardTag.find_by(tag: "fattoria").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/daf2c28e-472c-460b-92b0-46abc3889c65.jpeg")))
card = PresetCard.create!(id: "savana", label: "Savana", level: 3, card_tag_ids: [CardTag.find_by(tag: "savana").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/e16da842-56eb-46f8-ae7e-fcec7dba464d.jpeg")))
card = PresetCard.create!(id: "gallina", label: "Gallina", level: 3, card_tag_ids: [CardTag.find_by(tag: "gallina").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/269ad8af-c7be-417f-95bd-9db075cb3339.jpeg")))
card = PresetCard.create!(id: "mucca", label: "Mucca", level: 3, card_tag_ids: [CardTag.find_by(tag: "mucca").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/ab9163b3-3d9a-436f-b559-aff88ecb6ac3.jpeg")))
card = PresetCard.create!(id: "pecora", label: "Pecora", level: 3, card_tag_ids: [CardTag.find_by(tag: "pecora").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/51e33a8b-9107-4e6c-9699-1b77bb9cee99.jpeg")))
card = PresetCard.create!(id: "tigre", label: "Tigre", level: 3, card_tag_ids: [CardTag.find_by(tag: "tigre").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/9ecc155f-c257-4fd6-a918-91ff59d4622f.jpeg")))
card = PresetCard.create!(id: "leone", label: "Leone", level: 3, card_tag_ids: [CardTag.find_by(tag: "leone").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/1171d8c1-e6b9-42f3-a7dc-f730542a2bdd.jpeg")))
card = PresetCard.create!(id: "elefante", label: "Elefante", level: 3, card_tag_ids: [CardTag.find_by(tag: "elefante").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/218f78b7-12c5-45aa-96ea-984276d59271.jpeg")))
card = PresetCard.create!(id: "personaggibing", label: "Personaggibing", level: 3, card_tag_ids: [CardTag.find_by(tag: "personaggibing").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/3c589a7c-10bf-4ed4-bdc8-a0fe18027ee0.jpeg")))
card = PresetCard.create!(id: "bing", label: "Bing", level: 3, card_tag_ids: [CardTag.find_by(tag: "bing").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/632e733d-b106-4664-9904-646f6375a881.jpeg")))
card = PresetCard.create!(id: "mashaeorso", label: "Mashaeorso", level: 3, card_tag_ids: [CardTag.find_by(tag: "mashaeorso").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/ede7f9f4-2ba2-41cf-82c9-7fd98ba3d11a.jpeg")))
card = PresetCard.create!(id: "peppafamily", label: "Peppafamily", level: 3, card_tag_ids: [CardTag.find_by(tag: "peppafamily").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/714fb6be-8e64-44bb-b938-0f65cb786fc7.jpeg")))
card = PresetCard.create!(id: "masha", label: "Masha", level: 3, card_tag_ids: [CardTag.find_by(tag: "masha").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/aeb77254-57bf-42a0-8425-efcbd62cf8ef.jpeg")))
card = PresetCard.create!(id: "orso", label: "Orso", level: 3, card_tag_ids: [CardTag.find_by(tag: "orso").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/c60fec21-f9d2-4ed0-befb-aeaec431a86e.jpeg")))
card = PresetCard.create!(id: "peppa", label: "Peppa", level: 3, card_tag_ids: [CardTag.find_by(tag: "peppa").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/20e2cfc2-17f5-4ba9-8501-f219e3deff7b.jpeg")))
card = PresetCard.create!(id: "george", label: "George", level: 3, card_tag_ids: [CardTag.find_by(tag: "george").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/78b04497-461f-4a06-b915-41e27d86188b.jpeg")))
card = PresetCard.create!(id: "casa", label: "Casa", level: 3, card_tag_ids: [CardTag.find_by(tag: "casa").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/7565fa2f-d8a2-4c05-bd71-a44ad0b6e8e0.jpeg")))
card = PresetCard.create!(id: "animali", label: "Animali", level: 3, card_tag_ids: [CardTag.find_by(tag: "animali").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/9420e2d0-9081-4bec-99a0-4f8b02208367.jpeg")))
card = PresetCard.create!(id: "mattino", label: "MATTINO", level: 3, card_tag_ids: [CardTag.find_by(tag: "mattino").id,CardTag.find_by(tag: "giorno").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/083731d0-e232-4224-b523-185c96bf6723.jpeg")))
card = PresetCard.create!(id: "notte", label: "NOTTE", level: 3, card_tag_ids: [CardTag.find_by(tag: "notte").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/b7cd4ac1-66d2-475d-9591-913456622e52.jpeg")))
card = PresetCard.create!(id: "pioggia", label: "PIOGGIA", level: 3, card_tag_ids: [CardTag.find_by(tag: "pioggia").id,CardTag.find_by(tag: "nuvola").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/0caf56ec-63e7-4512-b405-4ceb19122ca6.jpeg")))
card = PresetCard.create!(id: "sole", label: "SOLE", level: 3, card_tag_ids: [CardTag.find_by(tag: "sole").id,CardTag.find_by(tag: "soleggiato").id,CardTag.find_by(tag: "giorno").id,CardTag.find_by(tag: "mattino").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/964c99f3-4274-4949-b3e1-bb4afabb890c.jpeg")))
card = PresetCard.create!(id: "adesso", label: "ADESSO", level: 3, card_tag_ids: [CardTag.find_by(tag: "adesso").id,CardTag.find_by(tag: "ora").id,CardTag.find_by(tag: "oggi").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/51bae9ef-32b6-41ee-af3a-5335b7326c3e.jpeg")))
card = PresetCard.create!(id: "io", label: "IO", level: 3, card_tag_ids: [CardTag.find_by(tag: "io").id,CardTag.find_by(tag: "me").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/fa8bb54b-cef8-4110-baa7-38fbea8c936b.jpeg")))
card = PresetCard.create!(id: "no... pensa bene", label: "No... Pensa bene", level: 3, card_tag_ids: [CardTag.find_by(tag: "no... pensa bene").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/9f8559e2-53b6-4ea7-b343-a607e9be1d85.jpeg")))
card = PresetCard.create!(id: "maestra", label: "MAESTRA", level: 3, card_tag_ids: [CardTag.find_by(tag: "maestra").id,CardTag.find_by(tag: "insegnante").id,CardTag.find_by(tag: "tutor").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/5575497b-4e11-4d00-b968-1d230a51f2c8.jpeg")))
card = PresetCard.create!(id: "dottore", label: "DOTTORE", level: 3, card_tag_ids: [CardTag.find_by(tag: "dottore").id,CardTag.find_by(tag: "medico").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/5ca59644-3c1e-4fca-b6cf-13178641f4d9.jpeg")))
card = PresetCard.create!(id: "bambini", label: "BAMBINI", level: 3, card_tag_ids: [CardTag.find_by(tag: "bambini").id,CardTag.find_by(tag: "bimbi").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/81901f04-ece4-41b1-a105-3953b20d6daf.jpeg")))
card = PresetCard.create!(id: "signora", label: "SIGNORA", level: 3, card_tag_ids: [CardTag.find_by(tag: "signora").id,CardTag.find_by(tag: "donna").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/df51ddfe-d00a-425b-aee1-c20d269d0592.jpeg")))
card = PresetCard.create!(id: "signore", label: "SIGNORE", level: 3, card_tag_ids: [CardTag.find_by(tag: "signore").id,CardTag.find_by(tag: "uomo").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/e5d84705-8335-4b5f-a031-10ceff71c327.jpeg")))
card = PresetCard.create!(id: "felice", label: "FELICE", level: 3, card_tag_ids: [CardTag.find_by(tag: "felice").id,CardTag.find_by(tag: "sorridente").id,CardTag.find_by(tag: "sorriso").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/4f7a7833-539c-4220-bd8d-0bffa3aa6017.jpeg")))
card = PresetCard.create!(id: "triste", label: "TRISTE", level: 3, card_tag_ids: [CardTag.find_by(tag: "triste").id,CardTag.find_by(tag: "tristezza").id,CardTag.find_by(tag: "infelice").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/f8d1e5c9-e28c-4f8b-8622-3f212b5c39d7.jpeg")))
card = PresetCard.create!(id: "arrabbiato", label: "ARRABBIATO", level: 3, card_tag_ids: [CardTag.find_by(tag: "arrabbiato").id,CardTag.find_by(tag: "rabbia").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/d51eec8e-0615-4421-bf79-235453dcb5da.jpeg")))
card = PresetCard.create!(id: "piangere", label: "PIANGERE", level: 3, card_tag_ids: [CardTag.find_by(tag: "piangere").id,CardTag.find_by(tag: "pianto").id,CardTag.find_by(tag: "lacrima").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/d3d1eb9e-e285-4bae-96de-428d71237bef.jpeg")))
card = PresetCard.create!(id: "spaventato", label: "SPAVENTATO", level: 3, card_tag_ids: [CardTag.find_by(tag: "spaventato").id,CardTag.find_by(tag: "spaventarsi").id,CardTag.find_by(tag: "spavento").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/f941de99-d9f3-4fef-bf17-f384dd130f06.jpeg")))
card = PresetCard.create!(id: "caldo", label: "CALDO", level: 3, card_tag_ids: [CardTag.find_by(tag: "caldo").id,CardTag.find_by(tag: "calore").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/c03ff810-8fbd-4fc5-9371-727bbb282f24.jpeg")))
card = PresetCard.create!(id: "capricci", label: "CAPRICCI", level: 3, card_tag_ids: [CardTag.find_by(tag: "capricci").id,CardTag.find_by(tag: "pestareipiedi").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/4a5c122b-7b41-4ce4-a572-b6f6b7edb82d.jpeg")))
card = PresetCard.create!(id: "ridere", label: "RIDERE", level: 3, card_tag_ids: [CardTag.find_by(tag: "ridere").id,CardTag.find_by(tag: "risata").id,CardTag.find_by(tag: "sorridere").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/6f484bdb-89e7-4170-a3ab-d2ac604405a8.jpeg")))
card = PresetCard.create!(id: "preoccupato", label: "PREOCCUPATO", level: 3, card_tag_ids: [CardTag.find_by(tag: "preoccupato").id,CardTag.find_by(tag: "nervoso").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/29eb2a48-d85b-4de7-a81f-b132751f53d5.jpeg")))
card = PresetCard.create!(id: "sto bene", label: "STO BENE", level: 3, card_tag_ids: [CardTag.find_by(tag: "sto bene").id,CardTag.find_by(tag: "tutto ok").id,CardTag.find_by(tag: "ok").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/aacf522b-d78f-4741-8506-a8d35fbbbd96.jpeg")))
card = PresetCard.create!(id: "no", label: "NO", level: 3, card_tag_ids: [CardTag.find_by(tag: "no").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/f9646c7b-be0f-4e97-a5f3-9d037e095839.jpeg")))
card = PresetCard.create!(id: "male", label: "MALE", level: 3, card_tag_ids: [CardTag.find_by(tag: "male").id,CardTag.find_by(tag: "giù").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/a000b691-11d7-437d-8ea4-c7d7173115e9.jpeg")))
card = PresetCard.create!(id: "noia", label: "NOIA", level: 3, card_tag_ids: [CardTag.find_by(tag: "noia").id,CardTag.find_by(tag: "annoiato").id,CardTag.find_by(tag: "infastidire").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/6185b731-5704-4de4-bf09-7bccafd37eae.jpeg")))
card = PresetCard.create!(id: "malato", label: "MALATO", level: 3, card_tag_ids: [CardTag.find_by(tag: "malato").id,CardTag.find_by(tag: "male").id,CardTag.find_by(tag: "dolore").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/6ef64d3e-0e2f-4e88-aa23-2596c8b19d64.jpeg")))
card = PresetCard.create!(id: "mal di testa", label: "MAL DI TESTA", level: 3, card_tag_ids: [CardTag.find_by(tag: "mal di testa").id,CardTag.find_by(tag: "male").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/06c3f22a-d0bd-4225-be0a-f192a57a7537.jpeg")))
card = PresetCard.create!(id: "stanco", label: "STANCO", level: 3, card_tag_ids: [CardTag.find_by(tag: "stanco").id,CardTag.find_by(tag: "sbadigliare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/553f6470-43d9-4098-a805-5a496cf44563.jpeg")))
card = PresetCard.create!(id: "bho", label: "BHO", level: 3, card_tag_ids: [CardTag.find_by(tag: "bho").id,CardTag.find_by(tag: "nonsapere").id,CardTag.find_by(tag: "iononso").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/c3dbfdbb-adf4-490d-90e8-33f16a6e5e84.jpeg")))
card = PresetCard.create!(id: "fame", label: "FAME", level: 3, card_tag_ids: [CardTag.find_by(tag: "fame").id,CardTag.find_by(tag: "mela").id,CardTag.find_by(tag: "cibo").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/a5938c31-c356-4f49-973c-b3c642a81785.jpeg")))
card = PresetCard.create!(id: "sete", label: "SETE", level: 3, card_tag_ids: [CardTag.find_by(tag: "sete").id,CardTag.find_by(tag: "bere").id,CardTag.find_by(tag: "acqua").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/8d335597-58c8-43d8-ab92-0976b1136b5f.jpeg")))
card = PresetCard.create!(id: "buono", label: "BUONO", level: 3, card_tag_ids: [CardTag.find_by(tag: "buono").id,CardTag.find_by(tag: "delizioso").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/9102287c-c916-45d0-b057-045e79b35d09.jpeg")))
card = PresetCard.create!(id: "tu", label: "TU", level: 3, card_tag_ids: [CardTag.find_by(tag: "tu").id,CardTag.find_by(tag: "lei").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/51d8af57-048f-4aec-8b73-f9605ac26ee1.jpeg")))
card = PresetCard.create!(id: "insieme", label: "INSIEME", level: 3, card_tag_ids: [CardTag.find_by(tag: "insieme").id,CardTag.find_by(tag: "prenderelamano").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/934651b0-0ceb-4cce-998f-8852320d959d.jpeg")))
card = PresetCard.create!(id: "sgridare", label: "SGRIDARE", level: 3, card_tag_ids: [CardTag.find_by(tag: "sgridare").id,CardTag.find_by(tag: "insultare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/7f77aab7-0913-4ce4-aef9-5c13af0fa636.jpeg")))
card = PresetCard.create!(id: "andare", label: "ANDARE", level: 3, card_tag_ids: [CardTag.find_by(tag: "andare").id,CardTag.find_by(tag: "passeggiare").id,CardTag.find_by(tag: "camminare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/c70f1589-f5b0-41ef-8a4d-aa24f274ddce.jpeg")))
card = PresetCard.create!(id: "farsi il bagno", label: "FARSI IL BAGNO", level: 3, card_tag_ids: [CardTag.find_by(tag: "farsi il bagno").id,CardTag.find_by(tag: "farebagno").id,CardTag.find_by(tag: "lavarsi").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/485f9d18-fa89-4ee1-8448-4cf697d86941.jpeg")))
card = PresetCard.create!(id: "lavare i denti", label: "LAVARE I DENTI", level: 3, card_tag_ids: [CardTag.find_by(tag: "lavare i denti").id,CardTag.find_by(tag: "lavarsiidenti").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/4a203c18-7b8c-46f1-92a4-2ac9b48b87fa.jpeg")))
card = PresetCard.create!(id: "lavare le mani", label: "LAVARE LE MANI", level: 3, card_tag_ids: [CardTag.find_by(tag: "lavare le mani").id,CardTag.find_by(tag: "lavarsilemani").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/2d5f5cca-0a92-4ce0-bada-00fc8a4da15a.jpeg")))
card = PresetCard.create!(id: "lavare i capelli", label: "LAVARE I CAPELLI", level: 3, card_tag_ids: [CardTag.find_by(tag: "lavare i capelli").id,CardTag.find_by(tag: "lavarsiicapelli").id,CardTag.find_by(tag: "shampoo").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/3b793897-158b-4aaf-b91f-5f59c59aadae.jpeg")))
card = PresetCard.create!(id: "leggere", label: "LEGGERE", level: 3, card_tag_ids: [CardTag.find_by(tag: "leggere").id,CardTag.find_by(tag: "libro").id,CardTag.find_by(tag: "lettura").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/5e0516e5-e98d-4f68-b4c4-b4d4db9a8193.jpeg")))
card = PresetCard.create!(id: "mangiare", label: "MANGIARE", level: 3, card_tag_ids: [CardTag.find_by(tag: "mangiare").id,CardTag.find_by(tag: "fame").id,CardTag.find_by(tag: "cibo").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/b7b98688-f61f-47c2-8483-f5829b2cd4ba.jpeg")))
card = PresetCard.create!(id: "mordere", label: "MORDERE", level: 3, card_tag_ids: [CardTag.find_by(tag: "mordere").id,CardTag.find_by(tag: "morso").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/f0d9e317-343d-477d-8482-b64e8b1c02ec.jpeg")))
card = PresetCard.create!(id: "cantare", label: "CANTARE", level: 3, card_tag_ids: [CardTag.find_by(tag: "cantare").id,CardTag.find_by(tag: "musica").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/f287bdde-8dfa-43eb-a665-ebb373b72d58.jpeg")))
card = PresetCard.create!(id: "osservare", label: "OSSERVARE", level: 3, card_tag_ids: [CardTag.find_by(tag: "osservare").id,CardTag.find_by(tag: "guardare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/1a3171b5-be3a-4ee6-a5a9-d469c7ea91ae.jpeg")))
card = PresetCard.create!(id: "correre", label: "CORRERE", level: 3, card_tag_ids: [CardTag.find_by(tag: "correre").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/e52df966-d013-49b1-a099-e3e61ee7cf34.jpeg")))
card = PresetCard.create!(id: "parlare", label: "PARLARE", level: 3, card_tag_ids: [CardTag.find_by(tag: "parlare").id,CardTag.find_by(tag: "conversare").id,CardTag.find_by(tag: "chiacchierare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/c0a9a545-239f-4a5b-910d-243dcc879b6e.jpeg")))
card = PresetCard.create!(id: "pensare", label: "PENSARE", level: 3, card_tag_ids: [CardTag.find_by(tag: "pensare").id,CardTag.find_by(tag: "pensiero").id,CardTag.find_by(tag: "penso").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/c8dfbb65-d8a5-4a98-b089-0f4b27b28ebc.jpeg")))
card = PresetCard.create!(id: "perdere", label: "PERDERE", level: 3, card_tag_ids: [CardTag.find_by(tag: "perdere").id,CardTag.find_by(tag: "perso").id,CardTag.find_by(tag: "sconfitta").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/f93b1ba6-888c-480c-b79a-d6b0bf090f50.jpeg")))
card = PresetCard.create!(id: "urlare", label: "URLARE", level: 3, card_tag_ids: [CardTag.find_by(tag: "urlare").id,CardTag.find_by(tag: "strillare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/5be7ae32-5de6-41fa-bb86-4e234203a455.jpeg")))
card = PresetCard.create!(id: "picchiare", label: "PICCHIARE", level: 3, card_tag_ids: [CardTag.find_by(tag: "picchiare").id,CardTag.find_by(tag: "picchiarsi").id,CardTag.find_by(tag: "schiaffo").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/44efb26e-ad87-4ba5-8d74-deb4cfc429fa.jpeg")))
card = PresetCard.create!(id: "disegnare", label: "DISEGNARE", level: 3, card_tag_ids: [CardTag.find_by(tag: "disegnare").id,CardTag.find_by(tag: "disegno").id,CardTag.find_by(tag: "scrivere").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/8668ed3a-7330-4196-9fdd-6d4b1a9af523.jpeg")))
card = PresetCard.create!(id: "ballare", label: "BALLARE", level: 3, card_tag_ids: [CardTag.find_by(tag: "ballare").id,CardTag.find_by(tag: "danzare").id,CardTag.find_by(tag: "ballo").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/cbfbe004-1ff9-4c5b-abb3-a074e08f280c.jpeg")))
card = PresetCard.create!(id: "aiutare", label: "AIUTARE", level: 3, card_tag_ids: [CardTag.find_by(tag: "aiutare").id,CardTag.find_by(tag: "aiuto").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/40c63294-a084-4887-a0db-7725f4218aa4.jpeg")))
card = PresetCard.create!(id: "coccole", label: "COCCOLE", level: 3, card_tag_ids: [CardTag.find_by(tag: "coccole").id,CardTag.find_by(tag: "coccolare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/c56284bd-fe90-44f5-8faa-3e12b6b932d3.jpeg")))
card = PresetCard.create!(id: "asciugare", label: "ASCIUGARE", level: 3, card_tag_ids: [CardTag.find_by(tag: "asciugare").id,CardTag.find_by(tag: "asciugarsi").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/a1aa8392-47cf-48a8-ab69-64a18e21dafe.jpeg")))
card = PresetCard.create!(id: "ascoltare", label: "ASCOLTARE", level: 3, card_tag_ids: [CardTag.find_by(tag: "ascoltare").id,CardTag.find_by(tag: "sentire").id,CardTag.find_by(tag: "ascolto").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/132b78e4-264f-4638-be66-6b4069659c35.jpeg")))
card = PresetCard.create!(id: "scuola", label: "SCUOLA", level: 3, card_tag_ids: [CardTag.find_by(tag: "scuola").id,CardTag.find_by(tag: "studiare").id,CardTag.find_by(tag: "giocare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/ca0fd385-5a3a-4449-b4e9-364ee5873989.jpeg")))
card = PresetCard.create!(id: "chiesa", label: "CHIESA", level: 3, card_tag_ids: [CardTag.find_by(tag: "chiesa").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/fb1d02c7-33f0-4e44-9508-8dd8ae44ec15.jpeg")))
card = PresetCard.create!(id: "piscina", label: "PISCINA", level: 3, card_tag_ids: [CardTag.find_by(tag: "piscina").id,CardTag.find_by(tag: "nuotare").id,CardTag.find_by(tag: "nuoto").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/89baa39c-8217-46aa-a818-2bc70021c87e.jpeg")))
card = PresetCard.create!(id: "parco", label: "PARCO", level: 3, card_tag_ids: [CardTag.find_by(tag: "parco").id,CardTag.find_by(tag: "parco giochi").id,CardTag.find_by(tag: "parchetto").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/60a0d614-077e-4625-9f5e-2afbf434ff67.jpeg")))
card = PresetCard.create!(id: "tog", label: "TOG", level: 3, card_tag_ids: [CardTag.find_by(tag: "tog").id,CardTag.find_by(tag: "amiciditog").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/d11b0de1-d0a8-49e3-a411-c7b87ee286b4.jpeg")))
card = PresetCard.create!(id: "mare", label: "MARE", level: 3, card_tag_ids: [CardTag.find_by(tag: "mare").id,CardTag.find_by(tag: "acqua").id,CardTag.find_by(tag: "bagno").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/2cb44d4c-0199-4d81-b223-2018f5c98454.jpeg")))
card = PresetCard.create!(id: "lago", label: "LAGO", level: 3, card_tag_ids: [CardTag.find_by(tag: "lago").id,CardTag.find_by(tag: "laghetto").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/b4c18888-3364-4879-b1bf-788810b03e0e.jpeg")))
card = PresetCard.create!(id: "montagna", label: "MONTAGNA", level: 3, card_tag_ids: [CardTag.find_by(tag: "montagna").id,CardTag.find_by(tag: "monte").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/8cba6e0a-1684-44bd-8aee-addf48a53823.jpeg")))
card = PresetCard.create!(id: "bagno", label: "BAGNO", level: 3, card_tag_ids: [CardTag.find_by(tag: "bagno").id,CardTag.find_by(tag: "andarealbagno").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/149b6c6f-56ea-42e4-9cb9-9da4af1680fb.jpeg")))
card = PresetCard.create!(id: "nuvoloso", label: "NUVOLOSO", level: 3, card_tag_ids: [CardTag.find_by(tag: "nuvoloso").id,CardTag.find_by(tag: "parzialmentenuvoloso").id,CardTag.find_by(tag: "nuvole").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/4e585a77-5d70-4257-b74c-8259374367ad.jpeg")))
card = PresetCard.create!(id: "biscotto", label: "BISCOTTO", level: 3, card_tag_ids: [CardTag.find_by(tag: "biscotto").id,CardTag.find_by(tag: "dolce").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/941f75c2-37ca-4ef7-86fb-c6fe0187940a.jpeg")))
card = PresetCard.create!(id: "frutta", label: "FRUTTA", level: 3, card_tag_ids: [CardTag.find_by(tag: "frutta").id,CardTag.find_by(tag: "pera").id,CardTag.find_by(tag: "banana").id,CardTag.find_by(tag: "arancia").id,CardTag.find_by(tag: "fragola").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/35e88b3a-b0ad-48f3-bbbe-7eea3aefac98.jpeg")))
card = PresetCard.create!(id: "verdura", label: "VERDURA", level: 3, card_tag_ids: [CardTag.find_by(tag: "verdura").id,CardTag.find_by(tag: "verdure").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/c24f41be-64f3-4fa1-97b6-3d0ef5c8fa3d.jpeg")))
card = PresetCard.create!(id: "gelato", label: "GELATO", level: 3, card_tag_ids: [CardTag.find_by(tag: "gelato").id,CardTag.find_by(tag: "cono").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/0a1e40d4-47ba-4f37-8aec-cd238d82f117.jpeg")))
card = PresetCard.create!(id: "pizza", label: "PIZZA", level: 3, card_tag_ids: [CardTag.find_by(tag: "pizza").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/7ad71832-b077-4826-b863-6b76354ba8a0.jpeg")))
card = PresetCard.create!(id: "patatine fritte", label: "PATATINE FRITTE", level: 3, card_tag_ids: [CardTag.find_by(tag: "patatine fritte").id,CardTag.find_by(tag: "patate").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/95c31d13-17a2-4937-8292-84cecf8754a9.jpeg")))
card = PresetCard.create!(id: "pane", label: "PANE", level: 3, card_tag_ids: [CardTag.find_by(tag: "pane").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/9e1f6ed9-6a46-4d24-8ae9-80f73917b1bb.jpeg")))
card = PresetCard.create!(id: "pappa", label: "PAPPA", level: 3, card_tag_ids: [CardTag.find_by(tag: "pappa").id,CardTag.find_by(tag: "darelapappa").id,CardTag.find_by(tag: "daredamangiare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/9a38544a-e551-4c18-86f7-f7bcce4ab90e.jpeg")))
card = PresetCard.create!(id: "compleanno", label: "COMPLEANNO", level: 3, card_tag_ids: [CardTag.find_by(tag: "compleanno").id,CardTag.find_by(tag: "festa").id,CardTag.find_by(tag: "torta").id,CardTag.find_by(tag: "candeline").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/e81f0ec8-9b79-4104-8798-ec77f5eb615f.jpeg")))
card = PresetCard.create!(id: "natale", label: "NATALE", level: 3, card_tag_ids: [CardTag.find_by(tag: "natale").id,CardTag.find_by(tag: "babbo natale").id,CardTag.find_by(tag: "alberodinatale").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/c904c9fe-e836-423f-87f2-1300b5c5fbad.jpeg")))
card = PresetCard.create!(id: "carnevale", label: "CARNEVALE", level: 3, card_tag_ids: [CardTag.find_by(tag: "carnevale").id,CardTag.find_by(tag: "maschere").id,CardTag.find_by(tag: "travestirsi").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/e52af614-28dd-4ac8-8f66-b02c85ed42b4.jpeg")))
card = PresetCard.create!(id: "soldi", label: "SOLDI", level: 3, card_tag_ids: [CardTag.find_by(tag: "soldi").id,CardTag.find_by(tag: "euro").id,CardTag.find_by(tag: "monete").id,CardTag.find_by(tag: "contanti").id,CardTag.find_by(tag: "denaro").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/9536d21c-971e-48d7-a82d-c7b5213f5054.jpeg")))
card = PresetCard.create!(id: "fuoco", label: "FUOCO", level: 3, card_tag_ids: [CardTag.find_by(tag: "fuoco").id,CardTag.find_by(tag: "fiamma").id,CardTag.find_by(tag: "falò").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/dcee04ba-4639-4c7c-85ff-10d9a3f35658.jpeg")))
card = PresetCard.create!(id: "dente", label: "DENTE", level: 3, card_tag_ids: [CardTag.find_by(tag: "dente").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/feb71343-e9e4-43fb-a77e-f10173afc1f8.jpeg")))
card = PresetCard.create!(id: "ospedale", label: "OSPEDALE", level: 3, card_tag_ids: [CardTag.find_by(tag: "ospedale").id,CardTag.find_by(tag: "crocerossa").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/e83d9c4e-b57b-4dfd-b99f-54e28586339b.jpeg")))
card = PresetCard.create!(id: "aereo", label: "AEREO", level: 3, card_tag_ids: [CardTag.find_by(tag: "aereo").id,CardTag.find_by(tag: "aeroplano").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/edab4302-b804-4130-87b2-0db8c6c1c09a.jpeg")))
card = PresetCard.create!(id: "bici", label: "BICI", level: 3, card_tag_ids: [CardTag.find_by(tag: "bici").id,CardTag.find_by(tag: "bicicletta").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/2136e8cd-01dc-4819-b2da-007f93ed5a26.jpeg")))
card = PresetCard.create!(id: "macchina", label: "MACCHINA", level: 3, card_tag_ids: [CardTag.find_by(tag: "macchina").id,CardTag.find_by(tag: "auto").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/54d3d9cc-4202-462d-8942-3b84c7c79648.jpeg")))
card = PresetCard.create!(id: "metro", label: "METRO", level: 3, card_tag_ids: [CardTag.find_by(tag: "metro").id,CardTag.find_by(tag: "metropolitana").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/de55f3c1-e607-4ff8-9a90-0876e5300184.jpeg")))
card = PresetCard.create!(id: "autobus", label: "AUTOBUS", level: 3, card_tag_ids: [CardTag.find_by(tag: "autobus").id,CardTag.find_by(tag: "bus").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/f5ef3d5e-a0f3-448a-b2a1-e2da59af1cb0.jpeg")))
card = PresetCard.create!(id: "nave", label: "NAVE", level: 3, card_tag_ids: [CardTag.find_by(tag: "nave").id,CardTag.find_by(tag: "barca").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/6d12d30e-b751-4769-bab2-2fc09bf9e7ff.jpeg")))
card = PresetCard.create!(id: "si", label: "SI", level: 3, card_tag_ids: [CardTag.find_by(tag: "si").id,CardTag.find_by(tag: "annuire").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/b3d8c4ed-4ccf-40c6-841f-628f1aa3d156.jpeg")))
card = PresetCard.create!(id: "treno", label: "TRENO", level: 3, card_tag_ids: [CardTag.find_by(tag: "treno").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/f2af9c24-b2a8-4a59-b58d-fa1afac89e99.jpeg")))
card = PresetCard.create!(id: "gatto", label: "GATTO", level: 3, card_tag_ids: [CardTag.find_by(tag: "gatto").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/0e0f09c2-28ce-443e-9bb8-603f389eda59.jpeg")))
card = PresetCard.create!(id: "anatra", label: "ANATRA", level: 3, card_tag_ids: [CardTag.find_by(tag: "anatra").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/f842559c-a2e1-47cc-931b-2e0955653d05.jpeg")))
card = PresetCard.create!(id: "asino", label: "ASINO", level: 3, card_tag_ids: [CardTag.find_by(tag: "asino").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/ac3bd546-c18e-4150-9bee-752372ad442c.jpeg")))
card = PresetCard.create!(id: "balena", label: "BALENA", level: 3, card_tag_ids: [CardTag.find_by(tag: "balena").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/5ca10486-7238-4c6b-8f78-16f64e552bf9.jpeg")))
card = PresetCard.create!(id: "cane", label: "CANE", level: 3, card_tag_ids: [CardTag.find_by(tag: "cane").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/92a53037-5227-4b17-9749-e60a54bfc438.jpeg")))
card = PresetCard.create!(id: "capra", label: "CAPRA", level: 3, card_tag_ids: [CardTag.find_by(tag: "capra").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/4111ccec-b754-4cc7-9f66-f7560032322f.jpeg")))
card = PresetCard.create!(id: "cavallo", label: "CAVALLO", level: 3, card_tag_ids: [CardTag.find_by(tag: "cavallo").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/de2a8d06-d7b8-4e29-a17e-e1e9e0555222.jpeg")))
card = PresetCard.create!(id: "coniglio", label: "CONIGLIO", level: 3, card_tag_ids: [CardTag.find_by(tag: "coniglio").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/d047ecf5-a1e0-48f4-bbce-42d164b4ddf5.jpeg")))
card = PresetCard.create!(id: "maiale", label: "MAIALE", level: 3, card_tag_ids: [CardTag.find_by(tag: "maiale").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/dbf55925-d377-403e-9026-ab36d11db435.jpeg")))
card = PresetCard.create!(id: "pesce", label: "PESCE", level: 3, card_tag_ids: [CardTag.find_by(tag: "pesce").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/9f49d35c-df78-479c-8aa2-55a1e14b383b.jpeg")))
card = PresetCard.create!(id: "serpente", label: "SERPENTE", level: 3, card_tag_ids: [CardTag.find_by(tag: "serpente").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/4e224e67-4647-4992-aca4-b49a178cf4a1.jpeg")))
card = PresetCard.create!(id: "squalo", label: "SQUALO", level: 3, card_tag_ids: [CardTag.find_by(tag: "squalo").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/68f7f256-00c8-46c7-822d-dd82e3309f14.jpeg")))
card = PresetCard.create!(id: "tartaruga", label: "TARTARUGA", level: 3, card_tag_ids: [CardTag.find_by(tag: "tartaruga").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/b05fa20b-5702-4f76-85eb-4c6b43f8d6e8.jpeg")))
card = PresetCard.create!(id: "topo", label: "TOPO", level: 3, card_tag_ids: [CardTag.find_by(tag: "topo").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/28f77df2-9b6b-4f00-92d4-b0201419dcd7.jpeg")))
card = PresetCard.create!(id: "uccello", label: "UCCELLO", level: 3, card_tag_ids: [CardTag.find_by(tag: "uccello").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/4104539e-5d38-4036-a367-4838d8474d53.jpeg")))
card = PresetCard.create!(id: "zebra", label: "ZEBRA", level: 3, card_tag_ids: [CardTag.find_by(tag: "zebra").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/0438f2a9-b7cc-474b-b20f-c67fac39a9b7.jpeg")))
card = PresetCard.create!(id: "x", label: "X", level: 3, card_tag_ids: [CardTag.find_by(tag: "x").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/ba62404a-d5b4-46fe-9214-a1533efdb4dd.jpeg")))
card = PresetCard.create!(id: "acqua", label: "ACQUA", level: 3, card_tag_ids: [CardTag.find_by(tag: "acqua").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/a3a20498-a9b0-4ce1-93cb-837c932932a0.jpeg")))
card = PresetCard.create!(id: "arancia", label: "ARANCIA", level: 3, card_tag_ids: [CardTag.find_by(tag: "arancia").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/367229d7-6235-4d63-a7de-726148df3c17.jpeg")))
card = PresetCard.create!(id: "banana", label: "BANANA", level: 3, card_tag_ids: [CardTag.find_by(tag: "banana").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/bea5fe2e-30e9-476c-8117-e847680cf4b2.jpeg")))
card = PresetCard.create!(id: "caramelle", label: "CARAMELLE", level: 3, card_tag_ids: [CardTag.find_by(tag: "caramelle").id,CardTag.find_by(tag: "dolci").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/6df223b2-cc47-4450-97b9-7a7b583400c7.jpeg")))
card = PresetCard.create!(id: "carne", label: "CARNE", level: 3, card_tag_ids: [CardTag.find_by(tag: "carne").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/a55d57fd-e0f1-447a-8454-c6e1e598817c.jpeg")))
card = PresetCard.create!(id: "carota", label: "CAROTA", level: 3, card_tag_ids: [CardTag.find_by(tag: "carota").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/f00c83ab-bd9a-4ca1-add3-45403b28c313.jpeg")))
card = PresetCard.create!(id: "cioccolato", label: "CIOCCOLATO", level: 3, card_tag_ids: [CardTag.find_by(tag: "cioccolato").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/19700d25-65cc-4901-ade5-0de3d5dbcc28.jpeg")))
card = PresetCard.create!(id: "coscia di pollo", label: "COSCIA DI POLLO", level: 3, card_tag_ids: [CardTag.find_by(tag: "coscia di pollo").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/d11b3c50-ae42-4e79-bfee-21cf195490cb.jpeg")))
card = PresetCard.create!(id: "croissant", label: "CROISSANT", level: 3, card_tag_ids: [CardTag.find_by(tag: "croissant").id,CardTag.find_by(tag: "cornetto").id,CardTag.find_by(tag: "brioche").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/6e9ac662-e9f1-490c-aee4-9335d0fccac3.jpeg")))
card = PresetCard.create!(id: "formaggio", label: "FORMAGGIO", level: 3, card_tag_ids: [CardTag.find_by(tag: "formaggio").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/f4afd485-c091-4d23-95b9-f10f0f88bed4.jpeg")))
card = PresetCard.create!(id: "fragola", label: "FRAGOLA", level: 3, card_tag_ids: [CardTag.find_by(tag: "fragola").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/81aa0a22-ce72-4a1b-bbf5-0b68cdc770cf.jpeg")))
card = PresetCard.create!(id: "frittata", label: "FRITTATA", level: 3, card_tag_ids: [CardTag.find_by(tag: "frittata").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/25ea10de-a4df-4f85-82c4-80fcd7d54bfd.jpeg")))
card = PresetCard.create!(id: "hamburger", label: "HAMBURGER", level: 3, card_tag_ids: [CardTag.find_by(tag: "hamburger").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/954fa077-29ce-48c4-b6fa-76733f57f5d5.jpeg")))
card = PresetCard.create!(id: "coca cola", label: "COCA COLA", level: 3, card_tag_ids: [CardTag.find_by(tag: "coca cola").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/554ec334-89b4-4cbb-acab-5f58ee3aea42.jpeg")))
card = PresetCard.create!(id: "ketchup", label: "KETCHUP", level: 3, card_tag_ids: [CardTag.find_by(tag: "ketchup").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/dc198867-5191-454d-8d67-d97d4bccbc04.jpeg")))
card = PresetCard.create!(id: "latte", label: "LATTE", level: 3, card_tag_ids: [CardTag.find_by(tag: "latte").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/76170414-bf99-4a5d-a8d4-de4bf035078e.jpeg")))
card = PresetCard.create!(id: "limone", label: "LIMONE", level: 3, card_tag_ids: [CardTag.find_by(tag: "limone").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/9bce5c7d-73d2-451b-96da-33d3b19cf05c.jpeg")))
card = PresetCard.create!(id: "mela", label: "MELA", level: 3, card_tag_ids: [CardTag.find_by(tag: "mela").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/011dbb5d-4e9b-4b1b-b395-95b8a1ec46be.jpeg")))
card = PresetCard.create!(id: "olio", label: "OLIO", level: 3, card_tag_ids: [CardTag.find_by(tag: "olio").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/50e3a655-9629-4397-91fb-344900342856.jpeg")))
card = PresetCard.create!(id: "patata", label: "PATATA", level: 3, card_tag_ids: [CardTag.find_by(tag: "patata").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/00ed6f0f-46d6-4263-9982-fb71dcc1f90b.jpeg")))
card = PresetCard.create!(id: "pasta", label: "PASTA", level: 3, card_tag_ids: [CardTag.find_by(tag: "pasta").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/be51dc01-7de7-40a3-acdc-0e7360d6e1d0.jpeg")))
card = PresetCard.create!(id: "polpette", label: "POLPETTE", level: 3, card_tag_ids: [CardTag.find_by(tag: "polpette").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/c2e10ca3-c659-4dd3-9cc1-c9204ba5cba8.jpeg")))
card = PresetCard.create!(id: "pomodoro", label: "POMODORO", level: 3, card_tag_ids: [CardTag.find_by(tag: "pomodoro").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/7f55802b-0c1b-4a05-b64d-9fed31a70aaf.jpeg")))
card = PresetCard.create!(id: "popcorn", label: "POPCORN", level: 3, card_tag_ids: [CardTag.find_by(tag: "popcorn").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/b0e3adf0-54c6-4e0c-b49a-5931e18c31a3.jpeg")))
card = PresetCard.create!(id: "prosciutto", label: "PROSCIUTTO", level: 3, card_tag_ids: [CardTag.find_by(tag: "prosciutto").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/50e671cc-fdc1-43fb-aad8-9b3e2152eed6.jpeg")))
card = PresetCard.create!(id: "riso", label: "RISO", level: 3, card_tag_ids: [CardTag.find_by(tag: "riso").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/ef975948-90ba-4b8b-b7fb-522707d6e6cc.jpeg")))
card = PresetCard.create!(id: "sale", label: "SALE", level: 3, card_tag_ids: [CardTag.find_by(tag: "sale").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/a48b04b2-a80e-4864-a88d-5670857b7b15.jpeg")))
card = PresetCard.create!(id: "torta", label: "TORTA", level: 3, card_tag_ids: [CardTag.find_by(tag: "torta").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/91ae0aaf-3e02-404c-81e1-98c58cf80cb7.jpeg")))
card = PresetCard.create!(id: "uovo", label: "UOVO", level: 3, card_tag_ids: [CardTag.find_by(tag: "uovo").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/75e21626-213c-45cb-9fcf-229a60ce92d9.jpeg")))
card = PresetCard.create!(id: "zucchero", label: "ZUCCHERO", level: 3, card_tag_ids: [CardTag.find_by(tag: "zucchero").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/a7fe4571-ecef-4383-8548-a76ed8f8d2d8.jpeg")))
card = PresetCard.create!(id: "zucchina", label: "ZUCCHINA", level: 3, card_tag_ids: [CardTag.find_by(tag: "zucchina").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/081cb4fa-f189-4ad5-af50-ba255e1a1ff6.jpeg")))
card = PresetCard.create!(id: "minestra", label: "MINESTRA", level: 3, card_tag_ids: [CardTag.find_by(tag: "minestra").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/e17993cd-c330-445a-a76c-caa7bd8477ab.jpeg")))
card = PresetCard.create!(id: "bimbi e mamma", label: "Bimbi e mamma", level: 3, card_tag_ids: [CardTag.find_by(tag: "bimbi e mamma").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/cce4a82c-9725-4ded-914e-24e0b7b79142.jpeg")))
card = PresetCard.create!(id: "bimbo spaventato", label: "Bimbo spaventato", level: 3, card_tag_ids: [CardTag.find_by(tag: "bimbo spaventato").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/907da93c-a154-4112-8e51-e3bae2c93786.jpeg")))
card = PresetCard.create!(id: "bambino piange", label: "Bambino piange", level: 3, card_tag_ids: [CardTag.find_by(tag: "bambino piange").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/57ee5038-c791-4048-a0b6-0e80dc883f93.jpeg")))
card = PresetCard.create!(id: "bambina triste", label: "Bambina triste", level: 3, card_tag_ids: [CardTag.find_by(tag: "bambina triste").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/62f9c2f2-fd02-4037-bf43-95f2f350700d.jpeg")))
card = PresetCard.create!(id: "bambina piange", label: "Bambina piange", level: 3, card_tag_ids: [CardTag.find_by(tag: "bambina piange").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/3cc1a1c0-24e0-4caf-84f4-480ee4beef5a.jpeg")))
card = PresetCard.create!(id: "parco giochi", label: "Parco giochi", level: 3, card_tag_ids: [CardTag.find_by(tag: "parco giochi").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/02af0b94-a0fa-457d-805c-50f2b222d775.jpeg")))
card = PresetCard.create!(id: "bambini mamma felici", label: "Bambini mamma felici", level: 3, card_tag_ids: [CardTag.find_by(tag: "bambini mamma felici").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/786ef573-1a40-41b2-8f47-683f27185428.jpeg")))
card = PresetCard.create!(id: "mamma", label: "Mamma", level: 3, card_tag_ids: [CardTag.find_by(tag: "mamma").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/765395ac-2450-489a-96c5-da3185f6120b.jpeg")))
card = PresetCard.create!(id: "bimbo felice", label: "Bimbo felice", level: 3, card_tag_ids: [CardTag.find_by(tag: "bimbo felice").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/5fe7e143-903b-45f4-a306-446f3fbd1aa3.jpeg")))
card = PresetCard.create!(id: "bambina felice", label: "Bambina felice", level: 3, card_tag_ids: [CardTag.find_by(tag: "bambina felice").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/7f98ffa8-3a63-4ec0-bb9f-ad424f4387ba.jpeg")))
card = PresetCard.create!(id: "mamma saluta binbi", label: "Mamma saluta binbi", level: 3, card_tag_ids: [CardTag.find_by(tag: "mamma saluta binbi").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/ab027aa5-446d-4758-9054-3ad960f3e7ed.jpeg")))
card = PresetCard.create!(id: "mamma esce", label: "Mamma esce", level: 3, card_tag_ids: [CardTag.find_by(tag: "mamma esce").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/5bfc480a-f150-4f2e-9f77-62c43b3172fb.jpeg")))
card = PresetCard.create!(id: "bambini salutano", label: "Bambini salutano", level: 3, card_tag_ids: [CardTag.find_by(tag: "bambini salutano").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/eb6e07fe-402a-4a82-bbbe-66d4cc89f1ce.jpeg")))
card = PresetCard.create!(id: "abbracciare", label: "Abbracciare", level: 3, card_tag_ids: [CardTag.find_by(tag: "abbracciare").id,CardTag.find_by(tag: "abbraccio").id,CardTag.find_by(tag: "coccole").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/007f37c0-02ff-437d-8ff3-2e62930bad6a.jpeg")))
card = PresetCard.create!(id: "nuotare", label: "Nuotare", level: 3, card_tag_ids: [CardTag.find_by(tag: "nuotare").id,CardTag.find_by(tag: "nuoto").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/6bebd03d-98c8-4122-92a0-1ffb4bdec0ad.jpeg")))
card = PresetCard.create!(id: "mettere", label: "Mettere", level: 3, card_tag_ids: [CardTag.find_by(tag: "mettere").id,CardTag.find_by(tag: "metti").id,CardTag.find_by(tag: "appoggiare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/45e8f622-1baf-40a3-b850-6e9b396879ac.jpeg")))
card = PresetCard.create!(id: "prendere", label: "Prendere", level: 3, card_tag_ids: [CardTag.find_by(tag: "prendere").id,CardTag.find_by(tag: "prendi").id,CardTag.find_by(tag: "raccogliere").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/b26662ae-cf0c-42a7-a8cc-75ebf00f57c0.jpeg")))
card = PresetCard.create!(id: "pulire", label: "Pulire", level: 3, card_tag_ids: [CardTag.find_by(tag: "pulire").id,CardTag.find_by(tag: "lavare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/2dfed79c-f1e8-44b5-aedd-9f0e24366049.jpeg")))
card = PresetCard.create!(id: "dormire", label: "Dormire", level: 3, card_tag_ids: [CardTag.find_by(tag: "dormire").id,CardTag.find_by(tag: "nanna").id,CardTag.find_by(tag: "sonno").id,CardTag.find_by(tag: "riposino").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/de720e09-a28b-4d5a-a3cf-656509c6aed8.jpeg")))
card = PresetCard.create!(id: "lanciare", label: "Lanciare", level: 3, card_tag_ids: [CardTag.find_by(tag: "lanciare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/f2225c2c-2ca0-4863-a196-44f70e7e3b36.jpeg")))
card = PresetCard.create!(id: "cucinare", label: "Cucinare", level: 3, card_tag_ids: [CardTag.find_by(tag: "cucinare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/bada5839-af2f-4be0-a9a9-46b022a89470.jpeg")))
card = PresetCard.create!(id: "sognare", label: "Sognare", level: 3, card_tag_ids: [CardTag.find_by(tag: "sognare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/c8c74fc7-c5e2-45e9-a86a-0ea12acaa95c.jpeg")))
card = PresetCard.create!(id: "incollare", label: "Incollare", level: 3, card_tag_ids: [CardTag.find_by(tag: "incollare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/1009c900-fdcd-4fdc-90d8-237f55d2428d.jpeg")))
card = PresetCard.create!(id: "tagliare", label: "Tagliare", level: 3, card_tag_ids: [CardTag.find_by(tag: "tagliare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/95a774fa-8904-44ee-8351-8dfb8763c617.jpeg")))
card = PresetCard.create!(id: "pregare", label: "Pregare", level: 3, card_tag_ids: [CardTag.find_by(tag: "pregare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/70b634a1-0831-4d35-855b-62d0f2d25d15.jpeg")))
card = PresetCard.create!(id: "avere", label: "Avere", level: 3, card_tag_ids: [CardTag.find_by(tag: "avere").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/632aed7f-aa1d-4d5d-86d3-716d6b920a26.jpeg")))
card = PresetCard.create!(id: "fare", label: "Fare", level: 3, card_tag_ids: [CardTag.find_by(tag: "fare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/72f31fe3-66f8-4c7f-851b-91f12659d6fe.jpeg")))
card = PresetCard.create!(id: "studiare", label: "Studiare", level: 3, card_tag_ids: [CardTag.find_by(tag: "studiare").id,CardTag.find_by(tag: "fareicompiti").id,CardTag.find_by(tag: "compiti").id,CardTag.find_by(tag: "studio").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/a62f443a-7af5-4a48-bd3d-766ecc1a7fe3.jpeg")))
card = PresetCard.create!(id: "supermercato", label: "Supermercato", level: 3, card_tag_ids: [CardTag.find_by(tag: "supermercato").id,CardTag.find_by(tag: "supermarket").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/1386afd6-467f-4afb-9d34-43fde1632c15.jpeg")))
card = PresetCard.create!(id: "parrucchiere", label: "Parrucchiere", level: 3, card_tag_ids: [CardTag.find_by(tag: "parrucchiere").id,CardTag.find_by(tag: "parrucchieria").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/c41c771f-1f87-4310-8449-ed680183d6c1.jpeg")))
card = PresetCard.create!(id: "giardino", label: "Giardino", level: 3, card_tag_ids: [CardTag.find_by(tag: "giardino").id,CardTag.find_by(tag: "giardinetto").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/607dd4f0-dba0-4842-a9cc-ba5cd0d904d7.jpeg")))
card = PresetCard.create!(id: "lavoro", label: "Lavoro", level: 3, card_tag_ids: [CardTag.find_by(tag: "lavoro").id,CardTag.find_by(tag: "ufficio").id,CardTag.find_by(tag: "lavorare").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/48a3b448-a0c0-49a0-8274-128aa2813649.jpeg")))
card = PresetCard.create!(id: "neve", label: "Neve", level: 3, card_tag_ids: [CardTag.find_by(tag: "neve").id,CardTag.find_by(tag: "nevicata").id,CardTag.find_by(tag: "nevicando").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/4da63cc8-1393-472d-9c61-5ed34dd41131.jpeg")))
card = PresetCard.create!(id: "prima", label: "Prima", level: 3, card_tag_ids: [CardTag.find_by(tag: "prima").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/1a5f3148-77a6-49aa-a06b-1b14b7abd4a0.jpeg")))
card = PresetCard.create!(id: "dopo", label: "Dopo", level: 3, card_tag_ids: [CardTag.find_by(tag: "dopo").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/2c8d9468-55ef-4a28-b0a9-0270ff8eadfe.jpeg")))
card = PresetCard.create!(id: "oggi", label: "Oggi", level: 3, card_tag_ids: [CardTag.find_by(tag: "oggi").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/0f0fd180-2bfc-46cb-a31c-5d393be123e8.jpeg")))
card = PresetCard.create!(id: "ieri", label: "Ieri", level: 3, card_tag_ids: [CardTag.find_by(tag: "ieri").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/404af294-4309-4241-8c06-60300eddc296.jpeg")))
card = PresetCard.create!(id: "domani", label: "Domani", level: 3, card_tag_ids: [CardTag.find_by(tag: "domani").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/d85f3825-0a6a-472f-9aff-1a4c00b7e376.jpeg")))
card = PresetCard.create!(id: "pasqua", label: "Pasqua", level: 3, card_tag_ids: [CardTag.find_by(tag: "pasqua").id,CardTag.find_by(tag: "uovo").id,CardTag.find_by(tag: "uovodipasqua").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/6daef183-3956-44e7-815f-0a0091549fcc.jpeg")))
card = PresetCard.create!(id: "festa", label: "Festa ", level: 3, card_tag_ids: [CardTag.find_by(tag: "festa").id,CardTag.find_by(tag: "festeggiare").id,CardTag.find_by(tag: "party").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/47fbcdad-c918-4b84-b303-85c93d2c9150.jpeg")))
card = PresetCard.create!(id: "carrozzina", label: "Carrozzina", level: 3, card_tag_ids: [CardTag.find_by(tag: "carrozzina").id,CardTag.find_by(tag: "sedia").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/45fc4468-6197-4549-82d2-317feaada00e.jpeg")))
card = PresetCard.create!(id: "calcio", label: "Calcio", level: 3, card_tag_ids: [CardTag.find_by(tag: "calcio").id,CardTag.find_by(tag: "giocareacalcio").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/44a2e817-1a37-4fb3-93db-9272179cd5b7.jpeg")))
card = PresetCard.create!(id: "nascondere", label: "Nascondere", level: 3, card_tag_ids: [CardTag.find_by(tag: "nascondere").id,CardTag.find_by(tag: "nascondino").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/86f0f01c-1f45-4c4d-81e7-5447b3d5d60f.jpeg")))
card = PresetCard.create!(id: "mascherina", label: "Mascherina ", level: 3, card_tag_ids: [CardTag.find_by(tag: "mascherina").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/28b63e84-9733-4cac-8317-899e3f155c5a.jpeg")))
card = PresetCard.create!(id: "virus", label: "Virus", level: 3, card_tag_ids: [CardTag.find_by(tag: "virus").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/8630f36f-7fbc-4950-bce5-53efeb001117.jpeg")))
card = PresetCard.create!(id: "palla", label: "Palla", level: 3, card_tag_ids: [CardTag.find_by(tag: "palla").id,CardTag.find_by(tag: "pallone").id,CardTag.find_by(tag: "calcio").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/ea018d80-232d-4b8d-87ab-f6736c9ab923.jpeg")))
card = PresetCard.create!(id: "bambola", label: "Bambola", level: 3, card_tag_ids: [CardTag.find_by(tag: "bambola").id,CardTag.find_by(tag: "bambolina").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/9c1af987-9e8a-4204-bca0-8566279dcf1f.jpeg")))
card = PresetCard.create!(id: "bolle di sapone", label: "Bolle di sapone ", level: 3, card_tag_ids: [CardTag.find_by(tag: "bolle di sapone").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/1313d49a-3bd6-42ca-a84f-313212f13655.jpeg")))
card = PresetCard.create!(id: "puzzle", label: "Puzzle", level: 3, card_tag_ids: [CardTag.find_by(tag: "puzzle").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/7302a1a3-29d9-43e8-b760-3c2240cbc11b.jpeg")))
card = PresetCard.create!(id: "palloncino", label: "Palloncino", level: 3, card_tag_ids: [CardTag.find_by(tag: "palloncino").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/fd84893e-bda3-4c80-bf3a-e68a050dc80f.jpeg")))
card = PresetCard.create!(id: "peluche", label: "Peluche", level: 3, card_tag_ids: [CardTag.find_by(tag: "peluche").id,CardTag.find_by(tag: "orsetto").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/5abfc7cb-3d5c-49d5-9ee6-211501ba43d0.jpeg")))
card = PresetCard.create!(id: "strumenti musicali", label: "Strumenti musicali ", level: 3, card_tag_ids: [CardTag.find_by(tag: "strumenti musicali").id,CardTag.find_by(tag: "strumenti").id,CardTag.find_by(tag: "musica").id],content_id: card_content.id)

puts card.inspect

card_content = GenericImage.create!(id: SecureRandom.uuid(), content: File.open(File.join(Rails.root, "/public/seed_images/6882bff2-5622-4a6f-b5ff-94840605d7e9.jpeg")))
card = PresetCard.create!(id: "libro", label: "Libro", level: 3, card_tag_ids: [CardTag.find_by(tag: "libro").id,CardTag.find_by(tag: "lettura").id,CardTag.find_by(tag: "leggere").id],content_id: card_content.id)

puts card.inspect
