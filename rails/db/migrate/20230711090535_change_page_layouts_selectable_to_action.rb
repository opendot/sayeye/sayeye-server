class ChangePageLayoutsSelectableToAction < ActiveRecord::Migration[5.1]
  def change
    add_column :page_layouts, :action, :integer, null: false, default: 0

    reversible do |dir|
      dir.up do
        execute <<-SQL
        	UPDATE page_layouts
        	SET action = CASE WHEN selectable THEN 0
        																		ELSE 1
        							 END;
        SQL
      end
      dir.down do
        execute <<-SQL
        	UPDATE page_layouts
        	SET selectable = (action != 1);
        SQL
      end
    end

    remove_column :page_layouts, :selectable, :boolean
  end
end
