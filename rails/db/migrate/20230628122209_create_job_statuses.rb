class CreateJobStatuses < ActiveRecord::Migration[5.1]
  def change
    create_table :job_statuses do |t|
      t.string :job_id, null: false, index: true
      t.string :job_type, null: false
      t.integer :status, null: false,  default: 0

      t.references :associated_obj, polymorphic: true, type: :string, optional: true
      t.string :result

      t.timestamps
    end
  end
end
