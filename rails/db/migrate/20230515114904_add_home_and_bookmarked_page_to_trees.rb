class AddHomeAndBookmarkedPageToTrees < ActiveRecord::Migration[5.1]
  def change
    add_column :trees, :home_page_id, :string
    add_column :trees, :bookmarked_page_id, :string
  end
end
