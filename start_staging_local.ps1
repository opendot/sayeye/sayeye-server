$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")

$dockercompose = "C:\Program Files\Docker\Docker\resources\bin\docker-compose.exe"

echo "Stopping Containers"
& $dockercompose -f docker-compose.staging_local.yml down

echo "Migrate"
& $dockercompose -f docker-compose.staging_local.yml run --rm api ruby bin/rails db:migrate RAILS_ENV=staging_local

echo "Preprocess personal images"
& $dockercompose -f docker-compose.staging_local.yml run --rm api ruby bin/rails sayeye:preprocess_personal_images RAILS_ENV=staging_local

echo "Starting Container"
& $dockercompose -f docker-compose.staging_local.yml up -d

