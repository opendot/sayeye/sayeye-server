$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")

$dockercompose = "C:\Program Files\Docker\Docker\resources\bin\docker-compose.exe"

echo "Stopping Containers"
& $dockercompose -f docker-compose.staging_local.yml down
