#!/bin/sh

rc boot
cd /home/sayeye/sayeye-rails-server/rails
git pull --tags https://gitlab.com/opendot/sayeye/sayeye-server.git
export SAYEYE_VERSION=`git describe --dirty --tags`

nohup /home/sayeye/ruby-2.5.9/bin/ruby /home/sayeye/ruby-2.5.9/bin/sidekiq -e staging_local >log/sidekiq.log &

rm -f tmp/pids/server.pid
/home/sayeye/ruby-2.5.9/bin/ruby bin/rails s -p 3001 -b '0.0.0.0' -e staging_local -d
